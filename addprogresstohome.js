import angular from 'angular'
import {_} from 'lodash'

angular.module('addprogresstohome', [])
    .config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
    $stateProvider
    .state('app.addprogresstohome', {
        url: '/addprogresstohome',
        controller: Controller
    })
}])


function Controller($rootScope, $state, $stateParams, $timeout) {

    let progressTaticsArrayFromDevice   = Device.info.user.progress.progressions
    if(_.keys(progressTaticsArrayFromDevice).length > 0){        
        if(_.keys(progressTaticsArrayFromDevice).length == 1){
            let taticsId = _.keys(progressTaticsArrayFromDevice)[0]
            let steps = Device.info.user.progress.progressions[taticsId].steps
            let totalSteps = Device.info.user.progress.progressions[taticsId].definition.steps
            let remainigSteps = _.keys(steps).length
            if((totalSteps.length - remainigSteps) == 0){
                $state.go('app.home')                 
            } else {
                $timeout(function () {
                    $rootScope.hiddenBootScreen = true
                    $state.go('app.progress', { progress: taticsId })
                }, 300)                 
            }
        }
        else {
            console.log('Enrolled in more then one progress tatics redirecting to default home state')
            $state.go('app.home')
        }

    }
    else {
        $state.go('app.home')
    }
    
}

Controller.$inject = ['$rootScope', '$state', '$stateParams', '$timeout']